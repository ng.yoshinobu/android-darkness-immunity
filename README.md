# Darkness Immunity

Android Darkness Immunity Digital Amulet. Install and it will protect you from darkness. Works like a charm! no ads, open-source, GPLv3!

Works even when phone is turned off (since values are still presents in current physical reality of phone memory module, you just can't see them when phone has no power).
 
DON'T PANIC!

You can handle it all. It just a dream. Love is the only one truth.

(reality)<-(lie)<-(love)->(lie)->(reality)

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Darkness Immunity' to your language or just add new speak engine please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)
