package com.github.axet.darknessimmunity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class LightDialog extends DialogFragment {
    TextView light;
    EditText edit;
    SeekBar seek;
    Result result;

    public static class Result implements DialogInterface {
        public int light;
        public float wl;

        public Result(Bundle args) {
            light = args.getInt("light");
            wl = args.getFloat("wl");
        }

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.lightdialog, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(v);
        builder.setTitle("Edit Color Channel");
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Bundle args = getArguments();
                float wl = args.getFloat("wl");
                int light = args.getInt("light");
                MainActivity.setLight(getContext(), light, wl);
                result = new Result(getArguments());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        Dialog d = builder.create();

        light = (TextView) v.findViewById(R.id.light);
        edit = (EditText) v.findViewById(R.id.edit);
        seek = (SeekBar) v.findViewById(R.id.seekbar);

        seek.setMax((MainActivity.MAX - MainActivity.MIN) * 10);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser)
                    return;
                getArguments().putFloat("wl", MainActivity.MIN + progress / 10f);
                updateText();
                updateEdit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = v.getText().toString();
                    float wl = Float.parseFloat(s);
                    if (wl < MainActivity.MIN)
                        wl = MainActivity.MIN;
                    if (wl > MainActivity.MAX)
                        wl = MainActivity.MAX;
                    v.setText("" + String.format("%.1f", wl));
                }
                return false;
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    float wl = Float.parseFloat(s.toString());
                    if (wl < MainActivity.MIN)
                        return;
                    if (wl > MainActivity.MAX)
                        return;
                    Bundle args = getArguments();
                    args.putFloat("wl", wl);
                    updateText();
                    updateProgress();
                } catch (NumberFormatException ignore) {
                }
            }
        });

        updateText();
        updateProgress();
        updateEdit();

        return d;
    }

    void updateEdit() {
        Bundle args = getArguments();
        float wl = args.getFloat("wl");
        edit.getText().clear();
        edit.append("" + wl);
    }

    void updateProgress() {
        Bundle args = getArguments();
        float wl = args.getFloat("wl");
        seek.setProgress((int) (wl - MainActivity.MIN) * 10);
    }

    void updateText() {
        Bundle args = getArguments();
        float wl = args.getFloat("wl");
        int rgb = MainApplication.waveLengthToRGB(wl);
        OvalShape shape = new OvalShape();
        ShapeDrawable bg = new ShapeDrawable();
        bg.setShape(shape);
        bg.getPaint().setColor(rgb);
        light.setBackgroundDrawable(bg);
        light.setText(String.format("%.1f nm", wl));
        light.startAnimation(new MainActivity.FadeTextAnimation(light, wl));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener) {
            if (result != null)
                ((DialogInterface.OnDismissListener) a).onDismiss(result);
        }
    }
}
